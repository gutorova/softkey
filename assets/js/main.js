$(document).ready(function () {


  $(".js-password").on('click', function(event) {
    let input = $(this).prev();
    input.attr('type', input.attr('type') === 'text' ? 'password' : 'text');
    if (input.attr('type') === 'text') {
      $(this).addClass('show-pass');
    } else {
      $(this).removeClass('show-pass');
    }
  });



  $(".list-group  a[href^='#']").on('click', function(e) {

    // prevent default anchor click behavior
    e.preventDefault();

    // animate
    let scroll_top = $(this.hash).offset().top - $(".header").height() - 62;
    if ($(window).width() < 768) {
      scroll_top += 40;
    }
    $('html, body').animate({
      scrollTop: scroll_top
    }, 100, function(){
      // when done, add hash to url
      // (default click behaviour)
      window.location.hash = this.hash;
    });

  });



  $('.js-btn-number').click(function(e){
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
      if(type == 'minus') {

        if(currentVal > input.attr('min')) {
          input.val(currentVal - 1).change();
        }
        if(parseInt(input.val()) == input.attr('min')) {
          $(this).attr('disabled', true);
        }

      } else if(type == 'plus') {

        if(currentVal < input.attr('max')) {
          input.val(currentVal + 1).change();
        }
        if(parseInt(input.val()) == input.attr('max')) {
          $(this).attr('disabled', true);
        }

      }
    } else {
      input.val(0);
    }
  });



  $('.js-input-number').focusin(function(){
    $(this).data('oldValue', $(this).val());
  })
  .change(function() {

    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());

    var name = $(this).attr('name');
    if(valueCurrent >= minValue) {
      $(".js-btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
      alert('Sorry, the minimum value was reached');
      $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
      $(".js-btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
      alert('Sorry, the maximum value was reached');
      $(this).val($(this).data('oldValue'));
    }
  });



  //промокод
  $("#checkpromo").change(function(){
    if ($("#checkpromo").is(":checked")){
      $("#promo").css('display', 'flex');
    }
    else{
      $("#promo").hide();
    }

  });



  $('.js-slider-main').flickity({
    // options
    cellAlign: 'left',
    contain: true,
    pageDots: true,
    adaptiveHeight: true
  });

  $('.js-slider-review').flickity({
    // options
    cellAlign: 'center',
    contain: true,
    groupCells: true,
    pageDots: false,
    adaptiveHeight: false
  });

  /* fix iOS touchmove issue */
  ;(function () {
    let touchingCarousel = false
    let startXCoordinate

    document.body.addEventListener('touchstart', e => {
      if (e.target.closest('.flickity-slider')) {
        touchingCarousel = true
      } else {
        touchingCarousel = false
        return
      }
      startXCoordinate = e.touches[0].pageX
    })

    document.body.addEventListener('touchmove', e => {
      if (
          !touchingCarousel ||
          !e.cancelable ||
          Math.abs(e.touches[0].pageX - startXCoordinate) < 8
      ) return
      e.preventDefault()
      e.stopPropagation()
    }, { passive: false })
  }())

  /* prevent resize in flickity if still animating */
  var resize = Flickity.prototype.resize;
  Flickity.prototype.resize = function() {
    if (! this.isAnimating) {
      resize.call( this );
    }
  };



  //пополнение баланса
  $(".js-add-balance").click(function () {
    $(this).parent().find('.js-add-balance').removeClass('active');
    $(this).addClass('active');

    $(this).parent().parent().find('.js-balance-input').val( $(this).data('value') );
  })

  $(function() {
    $(document).on("change keyup input click", ".js-balance-input, .js-input-number", function() {
      if(this.value.match(/[^0-9]/g)){
        this.value = this.value.replace(/[^0-9]/g, "");
      }
    });
  });



  var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
  var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
    return new bootstrap.Tooltip(tooltipTriggerEl)
  })

  $('.js-copy-link').click(function(){
    let link_input = $(this).parent().find('.js-copy-link-input');
    link_input.select();
    /* Copy the text inside the text field */
    document.execCommand("copy");

    // tooltip
    bootstrap.Tooltip.getInstance($(this)).hide();
    let tooltip = new bootstrap.Tooltip(link_input, {
      title: 'Текст ссылки скопирован',
      trigger: 'manual'
    });
    tooltip.show();
    window.setTimeout(function(){
      tooltip.hide();
    }, 1000);

    return false;
  });



  let header_dt = $('.header .nav-link.dropdown-toggle');
  header_dt.mouseenter(function(){
    header_dt.dropdown('hide');
    $('.nav-link.dropdown').dropdown('hide');
    $(this).dropdown('show');
  });



  // рейтинг звездочки
  $('.js-review div').click(function(){
    $(this).parent().find('div').removeClass('active');
    $(this).addClass('active');
    $(this).parent().find('input').val( $(this).data('rating') );
  });



  // табы в мобиле
  $('#selectsort').change(function(e){
    let optionSelected = $("option:selected", this);
    $('.tab-content div').removeClass('active');
    // $('.tab-content').each(function() {
    //   if($(this).find('div').hasClass('active')){
    //     $(this).find('div').removeClass('active');
    //     console.log("cfdfgdsg");
    //   }
    // });
    $('button[data-bs-target="#'+ optionSelected.data('id') +'"]').tab('show');
  });

  $('#toTop').click(function() {
    $('body,html').animate({scrollTop:0}, 300);
  });


});





